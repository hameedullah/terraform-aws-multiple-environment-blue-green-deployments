resource "aws_ecs_task_definition" "app" {
  family = "${var.environment}-app"
  container_definitions = "${file("setup/cluster/tasks/app.json")}"
}

resource "aws_elb" "app_elb" {
  name = "${var.environment}-elb"
  security_groups = [
    "${aws_security_group.elb_sg.id}"]
  subnets = [
    "${aws_subnet.main.id}"]


  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 443
    lb_protocol = "https"
    ssl_certificate_id = "${aws_iam_server_certificate.ssl_certificate.arn}"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:80/"
    interval = 5
  }

  connection_draining = true
  cross_zone_load_balancing = true

  tags {
    Name = "${var.environment}-elb"
  }
}

resource "aws_ecs_service" "app" {
  name = "${var.environment}-app"
  cluster = "${aws_ecs_cluster.cluster.id}"
  task_definition = "${aws_ecs_task_definition.app.arn}"
  desired_count = 1
  iam_role = "${aws_iam_role.cluster_service_role.arn}"
  depends_on = [
    "aws_iam_role_policy.cluster_service_role_policy"]
  load_balancer {
    elb_name = "${aws_elb.app_elb.id}"
    container_name = "app"
    container_port = 80
  }
}