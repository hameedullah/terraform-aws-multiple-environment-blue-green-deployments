variable "region" {
  type = "string"
  description = "AWS Region"
  default = "us-west-2"
}

variable "environment" {
  type = "string"
  description = "Environment Name"
  default = "test"
}

variable "random_cidr_key" {
  type = "string"
  description = "Random key"
  default = ""
}

variable "cidr_key" {
  type = "string"
  description = "Fixed key"
  default = "128"
}


variable "cidr_map" {
  type = "map"
  description = "CIDR Map"

  default = {
    test = "10.1.0.0/16"
    qa = "10.10.0.0/16"
    staging = "10.20.0.0/16"
    prod = "10.100.0.0/16"
  }
}

variable "ami" {
  type = "string"
  description = "ECS AMI"
  default = "ami-7abc111a"
}


variable "instance_type_map" {
  type = "map"
  description = "ECS Instance Type Map"
  default = {
    test = "t2.micro"
    qa = "t2.micro"
    staging = "t2.micro"
    prod = "t2.micro"
  }
}

variable "min_scale_map" {
  type = "map"
  description = "ECS Auto Scale Min Map"
  default = {
    test = 2
    qa = 2
    staging = 2
    prod = 2
  }
}

variable "max_scale_map" {
  type = "map"
  description = "ECS Auto Scale Max Map"
  default = {
    test = 4
    qa = 4
    staging = 4
    prod = 4
  }
}

variable "desired_scale_map" {
  type = "map"
  description = "ECS Auto Scale Desired Map"
  default = {
    test = 2
    qa = 2
  }
}

variable "disk_size" {
  description = "Size of the disks for EC2 Instances"
  default = 100
}

variable "ssh_public_key" {
  description = "Public Key used to ssh into the instances"
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDJQVO05nSZi/2IrhAQn+h1g+jb7BC1EVV4aPC/gqTdEC3irFM2SdLw9IzBxE1asvjBmEB2y+63W0Qxdbdm9ZBJSHSLVCIboEoBDQjSz9H58GI3QI5a7XlCqk2gOIBXSsXUjwJIxD0QYfx2NKwa8MRC42Fsp3v/oVSsmkHuryHXj9K5aAqFW/y7O7Iv2EZh9y8f/4lOAJdTIlHG2Z8NV9pLGDw/U4R+PwxpWBSfi2HZG69QJfSr28zL0g0kju8+r/gK6tX0flmMeQaa04YMQPe7pidoukl9rVeVXymCS6j+0QzYHdV4Q9e7nc0gUIUeFO2gCn1NOU8k94MXgliYsDc8/QvDBmwEbc3PK4I94O+UphcoPiasOKQQoskO0tvlMfUDeirRGU1hWlLU3OWZeAAgYXefzTx8RfFnnVhz48ODqHffm97XBHpYeR5A0wMfBXSdWa9sN599s3xtEYy/UQTK9FcqZRhDAxDQA+LA7z/ycvrq53bAb9DKH0nohPgE8gMszJoqIcUgS2XNsSgB4xSZPGgTz8kWzMLT6yH+jK4dIRblF7j6Gc/YajZMbV0zxYeGVjgOQKY3AioCeXWZzbbZ/JMGzDJSNN7tReNBMEpB7OMDQf2YcIBYZ3Oo4eSiUnWN6ggvF8cwJ6ddZ2PBhuYZKJYw9d1aOo16W0UEzbDNUQ== oms-infra
"
}

variable "key_name"{
  description = "Your Key Name"
  default = "REPLACE_ME"
}

