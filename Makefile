.PHONY: all test-create
UNAME := $(shell uname)
ifeq ($(UNAME),Linux)
	TERRAFORM_URL="https://releases.hashicorp.com/terraform/0.7.11/terraform_0.7.11_linux_amd64.zip"
endif
ifeq ($(UNAME),Darwin)
	TERRAFORM_URL="https://releases.hashicorp.com/terraform/0.7.11/terraform_0.7.11_darwin_amd64.zip"
endif

RANDOM_NUMBER:=$(shell python -c "import random; print random.randint(1,255)")

define commit_state
	git add .states;git commit -m "Updating terraform state for the environment $(1)";git push origin master;
endef

define create
	terraform apply -var 'environment=$(1)' -state='.states/$(1).tfstate' -state-out='.states/$(1).tfstate' setup/cluster;
	$(call commit_state,$(1))
endef

define plan
	terraform plan -var 'environment=$(1)' -state='.states/$(1).tfstate' setup/cluster;
endef


define destroy
	terraform destroy -var 'environment=$(1)' -state='.states/$(1).tfstate' -state-out='.states/$(1).tfstate' setup/cluster;
	$(call commit_state,$(1))
endef

install:
	rm -rf terraform terraform.zip
	curl -L $(TERRAFORM_URL) > terraform.zip
	unzip terraform.zip
	rm -rf terraform.zip
	mv terraform /usr/local/bin/terraform

test-plan:
	$(call plan,test)

test-create:
	$(call create,test)

test-destroy:
	$(call destroy,test)

qa-plan:
	$(call plan,qa)

qa-create:
	$(call create,qa)

qa-destroy:
	$(call destroy,qa)

staging-plan:
	$(call plan,staging)

staging-create:
	$(call create,staging)

staging-destroy:
	$(call destroy,staging)

prod-plan:
	$(call plan,prod)

prod-create:
	$(call create,prod)

temp-plan:
	@echo This is your instance id $(RANDOM_NUMBER)
	terraform plan -var 'random_cidr_key=$(RANDOM_NUMBER)' -var 'environment=temp$(RANDOM_NUMBER)' -state='.states/temp$(RANDOM_NUMBER).tfstate' setup/cluster
	@echo This is your instance id $(RANDOM_NUMBER)

temp-create:
	@echo This is your instance id $(RANDOM_NUMBER)
	terraform apply -var 'random_cidr_key=$(RANDOM_NUMBER)' -var 'environment=temp$(RANDOM_NUMBER)' -state-out='.states/temp$(RANDOM_NUMBER).tfstate' -state='.states/temp$(RANDOM_NUMBER).tfstate' setup/cluster
	@echo This is your instance id $(RANDOM_NUMBER)

instance-plan:
	@echo Instance id is $(INST_ID)
	terraform plan -var 'random_cidr_key=$(INST_ID)' -var 'environment=temp$(INST_ID)' -state='.states/temp$(INST_ID).tfstate' setup/cluster

instance-create:
	@echo Instance id is $(INST_ID)
	terraform apply -var 'random_cidr_key=$(INST_ID)' -var 'environment=temp$(INST_ID)' -state='.states/temp$(INST_ID).tfstate' setup/cluster

instance-destroy:
	@echo Instance id is $(INST_ID)
	terraform destroy -var 'random_cidr_key=$(INST_ID)' -var 'environment=temp$(INST_ID)' -state='.states/temp$(INST_ID).tfstate' setup/cluster